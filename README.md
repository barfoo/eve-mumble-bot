**Eve mumble bot**
==================
Written by drymer@daemons.cf and foobar@openmailbox.org under GNU GPL License

**Description:**
------------------
This code uses the mumble-ruby libraries for make a controlable bot for mpd (music player daemon) on mumble servers,
through the mumble chat. 
https://github.com/perrym5/mumble-ruby

	
**Usage:**
-----------------
**Initialize the bot and connect it to mumble server:**

./eve.rb --name namebot --owner owner --server elbinario.net --port 60601 --channel channel --key password


Use --owner free for using all users in channel. quit command inst allowed in this way.


**Mumble chat usage:**
-------------------

**Using help:**

Type **!help** for help in the same channel were bot has join
You'll see this help:

 !play  - Play music 

 !current - Current song

 !stop - Stop music

 !next - Next song

 !random - Random play

 !pause - Pause music

 !quit - Disconnect and exit 


That's all!!! 
More info how to setup mpd and this bot here:   
http://elbinario.net/2014/12/02/eve-mumble-bot/
