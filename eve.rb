#!/usr/bin/env ruby
# -*- encoding : utf-8 -*-

require 'mumble-ruby'
require 'optparse'

options = {:name => nil, :owner => nil, :server => nil, :port => nil, :channel => nil, :key => nil}

parser = OptionParser.new do|opts|
	opts.banner = "'./eve.rb namebot owner elbinario.net 60601 password'"
	opts.on('-n', '--name name', 'Name') do |name|
		options[:name] = name;
	end

	opts.on('-o', '--owner owner', 'Owner') do |owner|
		options[:owner] = owner;
	end
	
	opts.on('-s', '--server server', 'Server') do |server|
		options[:server] = server;
	end
	
	opts.on('-p', '--port port', 'Port') do |port|
		options[:port] = port;
	end
	
	opts.on('-c', '--channel channel', 'Channel') do |channel|
		options[:channel] = channel;
	end
	
	opts.on('-k', '--key key', 'Key') do |key|
		options[:key] = key;
	end
	
  	opts.on('-h', '--help', './eve.rb namebot owner elbinario.net 60601 password') do
		puts opts
		exit
	end
end

parser.parse!

if options[:name] == nil
	print 'Enter Name: '
    options[:name] = gets.chomp
end

if options[:owner] == nil
	print 'Enter Owner: '
    options[:owner] = gets.chomp
end

if options[:server] == nil
	print 'Enter Server: '
    options[:server] = gets.chomp
end

if options[:port] == nil
	print 'Enter Port: '
    options[:port] = gets.chomp
end

if options[:channel] == nil
	print 'Enter Channel: '
    options[:channel] = gets.chomp
end

if options[:key] == nil
	print 'Enter Key press enter if dont required: '
    options[:key] = gets.chomp
end


if options[:key] != ""
	cli = Mumble::Client.new(options[:server], options[:port], options[:name], options[:key])
else
	cli = Mumble::Client.new(options[:server], options[:port], options[:name])
end	

cli.connect

sleep(1)

#Join channel
begin
  cli.join_channel(options[:channel])
rescue
  puts "This channel does not exist! Going to the root."
end

owner = options[:owner]
out = 0
pause = false
#Associate owner's name with owner's session_id
cli.users.values.find do|user|
  if user.name == owner
    owner = user.session
  end
end

cli.on_text_message do |msg|
  if msg.message == "!help"
    cli.text_user(msg.actor ,'!play - Play music')
    cli.text_user(msg.actor ,"!current - Current song")
    cli.text_user(msg.actor ,"!next - Next song")
    cli.text_user(msg.actor ,"!random - Random play")
    cli.text_user(msg.actor ,"!toggle - Toggle music")
    cli.text_user(msg.actor ,"!stop - Stop music")
    cli.text_user(msg.actor ,"!quit - Disconnect bot")
end
 if msg.message == "!play" && (msg.actor == owner || owner == "free")
    if pause == false
       cli.player.stream_named_pipe('/tmp/mumble.fifo')
       play = `mpc play 1`
       action = `mpc current`
   else 
      action = `mpc toggle`
      action = `mpc current`
      pause = false
   end 
   cli.text_user(msg.actor , action)
end

  if msg.message == "!current" && (msg.actor == owner || owner == "free")
    action = `mpc current`
    cli.text_user(msg.actor , action)
  end
  if msg.message == "!next" && (msg.actor == owner || owner == "free")
    action = `mpc next`
    cli.text_user(msg.actor , action)
  end
  if msg.message == "!random" && (msg.actor == owner || owner == "free")
    action = `mpc random`
    action = `mpc next`
    cli.text_user(msg.actor , action)
  end
  if msg.message == "!pause" && (msg.actor == owner || owner == "free")
    if pause == false
       action = `mpc toggle`
       pause = true
       cli.text_user(msg.actor , action)
   end
  end
  if msg.message == "!stop" && (msg.actor == owner || owner == "free")
    action = `mpc stop`
    cli.text_user(msg.actor , action)
  end
  if msg.message == "!quit" && msg.actor == owner
    action = "bye bye"
    cli.text_user(msg.actor , action)
    out = 1
    break
  end
end

if out == 1
  cli.disconnect
end  

print 'Press enter to terminate script';
gets

cli.disconnect

